import argparse
import csv
import math

# pick one of the below for your down payment
DOWN_PAYMENT_PERCENTAGE = 0.20
DOWN_PAYMENT_AMOUNT = None # 200000

RENT_AMOUNT = 3600
TAX_RATE = 0.3
MORTGAGE_YEARS = 30
MORTAGE_MONTHLY_INTEREST_RATE = 0.03875 / 12

# san francisco values
PURCHASING_CLOSING_COST_AS_PERCENT_OF_PRICE = 0.007
SELLING_CLOSING_COST_AS_PERCENT_OF_PRICE = 0.065

# inflation rate is used to increase rent, house value, HoA fees
INFLATION_RATE = 1.02

MORTGAGE_FEES = 775
HOME_INSURANCE_RATE = 0.0022
PROPERTY_TAX_RATE = 0.0122
ANNUAL_MAINTENANCE_PERCENTAGE_FEE = 0.015

def get_downpayment_from_price(price):
    if DOWN_PAYMENT_AMOUNT != None:
        down_payment_amount = DOWN_PAYMENT_AMOUNT
    else:
        down_payment_amount = DOWN_PAYMENT_PERCENTAGE * price
    return down_payment_amount

def monthly_mortgage(loan, interest, numberOfPayments):
    return loan * (interest * (1 + interest) ** numberOfPayments) / ((1 + interest) ** numberOfPayments - 1)

def monthly_mortgage_from_price(price):
    down_payment_amount = get_downpayment_from_price(price)
    return monthly_mortgage(price - down_payment_amount, MORTAGE_MONTHLY_INTEREST_RATE, MORTGAGE_YEARS * 12.0)

# TODO take into account investment income from payment if renting (opportunity cost of downpay)
def years_to_breakeven(price, HOA):
    price = float(price)
    HOA = float(HOA)

    cost_of_renting = 0.0
    cost_of_buying = 0.0

    downpayment = float(get_downpayment_from_price(price))
    amount_equity = downpayment
    remaining_mortgage = price - downpayment
    amount_interest_paid = 0.0

    # purchase house costs
    cost_of_buying -= downpayment
    cost_of_buying -= MORTGAGE_FEES
    cost_of_buying -= PURCHASING_CLOSING_COST_AS_PERCENT_OF_PRICE * price

    value_of_house = price
    rent_amount = RENT_AMOUNT
    home_owners_association_fee = HOA

    home_insurance_monthly_fee = HOME_INSURANCE_RATE * value_of_house / 12.0
    monthly_property_tax = PROPERTY_TAX_RATE * value_of_house / 12.0
    monthly_mortgage_payment = monthly_mortgage_from_price(value_of_house)

    for month in range(0, 30 * 12):
        cost_of_renting -= rent_amount
        cost_of_buying -= home_owners_association_fee + home_insurance_monthly_fee + monthly_property_tax + monthly_mortgage_payment

        mortgage_interest_for_month = remaining_mortgage * MORTAGE_MONTHLY_INTEREST_RATE
        amount_interest_paid += mortgage_interest_for_month
        amount_equity += monthly_mortgage_payment - mortgage_interest_for_month
        remaining_mortgage -= monthly_mortgage_payment - mortgage_interest_for_month

        # pretend we sell house, did we break even yet?
        closing_costs = SELLING_CLOSING_COST_AS_PERCENT_OF_PRICE * value_of_house
        equity_from_sale_of_house = (amount_equity / price * value_of_house) * (INFLATION_RATE ** math.floor(month / 12))
        tax_savings = amount_interest_paid * TAX_RATE
        if cost_of_buying - closing_costs + equity_from_sale_of_house + tax_savings >= cost_of_renting:
            return month / 12.0

        # adjust for inflation / appreciation at year end
        if month % 12 == 0:
            value_of_house *= INFLATION_RATE
            rent_amount *= INFLATION_RATE
            home_insurance_monthly_fee *= INFLATION_RATE
            monthly_property_tax *= INFLATION_RATE
            home_owners_association_fee *= INFLATION_RATE

    return 30

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("redfin_csv", help="Redfin CSV to filter/aggregate data from")
    args = parser.parse_args()

    reader = csv.DictReader(open(args.redfin_csv))
    houses = []
    for row in reader:
        sale_type = row['SALE TYPE']

        # TODO: make basic filtering easier to do
        if sale_type == 'New Construction Plan':
            continue
        district = row['LOCATION']
        if district in []:
            continue

        price = float(row['PRICE'])
        beds = row['BEDS']
        baths = row['BATHS']
        year_built = row['YEAR BUILT']
        sqft = row['SQFT']
        days_on_market = row['DAYS ON MARKET']
        url = row['URL (SEE http://www.redfin.com/buy-a-home/comparative-market-analysis FOR INFO ON PRICING)']
        HOA = float(row['HOA'])

        home_insurance_monthly_fee = HOME_INSURANCE_RATE * price / 12.0
        monthly_property_tax = PROPERTY_TAX_RATE * price / 12.0
        monthly_mortgage_payment = monthly_mortgage_from_price(price)
        total_monthly_cost = HOA + home_insurance_monthly_fee + monthly_property_tax + monthly_mortgage_payment

        if total_monthly_cost > 6000:
            continue

        breakeven_years = years_to_breakeven(price, HOA)
        houses.append({
            'district': district,
            'beds': beds,
            'baths': baths,
            'url': url,
            'total_monthly_cost': total_monthly_cost,
            'breakeven_years': breakeven_years
        })

    sorted_houses = sorted(houses, key=lambda x: x['breakeven_years'])
    for house in sorted_houses:
        print house

if __name__ == "__main__":
    main()
