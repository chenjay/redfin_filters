import argparse
import random
import requests
import time
import os.path

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, sdch, br',
    'Accept-Language' :'en-US,en;q=0.8',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive'
}

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("redfin_csv", help="Redfin CSV to scrape listings data")
    args = parser.parse_args()
    with open(args.redfin_csv, 'r') as csvfile:
        # ['SALE TYPE', 'PROPERTY TYPE', 'ADDRESS', 'CITY', 'STATE', 'ZIP', 'PRICE', 'BEDS', 'BATHS', 'LOCATION', 'SQFT', 'LOT SIZE', 'YEAR BUILT', 'DAYS ON MARKET', 'STATUS', 'NEXT OPEN HOUSE START TIME', 'NEXT OPEN HOUSE END TIME', 'URL (SEE http://www.redfin.com/buy-a-home/comparative-market-analysis FOR INFO ON PRICING)', 'SOURCE', 'LISTING ID', 'FAVORITE', 'INTERESTED', 'LATITUDE', 'LONGITUDE']
        header = csvfile.readline()
        for row in csvfile:
            row = row.split(',')
            listing_id = row[-5]
            url = row[17]
            if os.path.isfile('data/' + listing_id):
                print 'Skipping', listing_id
                continue
            print 'Found missing', listing_id
            r = requests.get(url, headers=HEADERS)
            temp_file = open('data/' + listing_id, 'w')
            temp_file.write(r.text.encode('utf8', 'replace'))
            temp_file.close()
            time.sleep(random.randint(10, 60))

if __name__ == "__main__":
    main()
