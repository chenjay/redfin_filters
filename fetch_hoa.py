import argparse

def find_hoa_value(string):
    HOA_KEY = 'HOA Dues\\",\\"content\\":\\"$'
    index = string.find(HOA_KEY)
    if index == -1:
        return '0'

    hoa_value_index_start = index + len(HOA_KEY)
    hoa_value_str = string[hoa_value_index_start : hoa_value_index_start + 10]
    hoa_value = ''
    for char in hoa_value_str:
        if ord(char) == 92:
            break
        hoa_value += str(char)
    return hoa_value

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_csv", help="Input Redfin CSV file", required=True)
    parser.add_argument("--output_csv", help="Output Redfin CSV file with included HoA fees", required=True)
    args = parser.parse_args()
    csv_with_hoa = open(args.output_csv, 'w')
    with open(args.input_csv, 'r') as csvfile:
        # ['SALE TYPE', 'PROPERTY TYPE', 'ADDRESS', 'CITY', 'STATE', 'ZIP', 'PRICE', 'BEDS', 'BATHS', 'LOCATION', 'SQFT', 'LOT SIZE', 'YEAR BUILT', 'DAYS ON MARKET', 'STATUS', 'NEXT OPEN HOUSE START TIME', 'NEXT OPEN HOUSE END TIME', 'URL (SEE http://www.redfin.com/buy-a-home/comparative-market-analysis FOR INFO ON PRICING)', 'SOURCE', 'LISTING ID', 'FAVORITE', 'INTERESTED', 'LATITUDE', 'LONGITUDE']
        header = csvfile.readline()
        csv_with_hoa.write(header.strip() + ",HOA\n")
        for row in csvfile:
            row_split = row.split(',')
            listing_id = row_split[-5]
            redfine_page_file = open('data/' + listing_id, 'r')
            redfin_page = redfine_page_file.read()
            hoa_value = find_hoa_value(redfin_page)
            csv_with_hoa.write(row.strip() + ',' + hoa_value + '\n')
            redfine_page_file.close()
        csv_with_hoa.close()

if __name__ == "__main__":
    main()
