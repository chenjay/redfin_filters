## Redfin Filters

The motivation of this project is to make it easier to filter and sort through many Redfin
candidates for your future home. By applying some strict filtering and then some basic sorting,
you can now get a much smaller list of houses to consider!

The sorting function is easy to change, but for now it sorts on how many years it will take someone
to break even from renting versus buying a specific house from a Redfin listing. The methodology for computing
how many years it will take to breakeven on buying a house versus renting was taken from https://smartasset.com/mortgage/rent-vs-buy.
The reason we chose this for our sorting function is because one of the scary parts of buying a house is
the commitment to staying in one location for a long period of time.
Giving up on that commitment early might be financially expensive, but now we can view candidates
knowing approximate how long that commitment will be.

There already exists complete files to get a sense of exactly what this looks like.

> python aggregate.py redfin_csv/redfin_2016-12-03-14-33-34-with_hoa.csv

At the top of `aggregate.py`, you can find all the parameters used to compute the breakeven point.
The ones more specific to an individual's scenario are found at the top: downpayment, rent, tax rate, mortgage years, mortgage interest rate.

Filtering is unfortunately currently more difficult to add as it is extremely depenendent on the information from your CSV.
The code in main is fortunately simple, so please add whatever filters you want to apply there, such as neighborhood
whitelisting/blacklisting or maximum monthly payments.

## Requirements

The only external dependency for this repo is `requests`. If you don't already have them installed globally:

> $ virtualenv env
> $ source env/bin/activate
> $ pip install -r requirements.txt

## To run on existing data

This repo already includes some SF housing data from 12/4/2016. To run with the existing parameters and the existing SF
housing data:

> python aggregate.py redfin_csv/redfin_2016-12-03-14-33-34-with_hoa.csv

To run this yourself on some downloaded CSV file from Redfin:

1. First we need to fetch the home owners association fee for each listing. This is not available for download by Redfin so
we fetch the HTML pages ourselves to scrape it ourselves:

> python download.py redfin_csv/redfin_2016-12-03-14-33-34.csv

2. Now that we have the HTML pages, we can fetch the HoA files and append the information into our existing CSV:

> python fetch_hoa.py --input_csv redfin_csv/redfin_2016-12-03-14-33-34.csv --output_csv redfin_2016-12-03-14-33-34-with_hoa.csv

3. Run the filtering / aggregation.

At the top of `aggregate.py`, you can find all the parameters used to compute the breakeven point.
The ones more specific to your scenario are found at the top: downpayment, rent, tax rate, mortgage years, mortgage interest rate.

Filtering is unfortunately currently more difficult to add as it is extremely depenendent on the information from your CSV.
The code in main is fortunately simple, so please add whatever filters you want to apply there, such as neighborhood
whitelisting/blacklisting or maximum monthly payments on houses.

> python aggregate.py redfin_csv/redfin_2016-12-03-14-33-34-with_hoa.csv
